
namespace MetaballsTest {
    public class Application : Adw.Application {
        public Application () {
            Object (application_id: "org.gnome.Example", flags: ApplicationFlags.FLAGS_NONE);
        }

        construct {
            ActionEntry[] action_entries = {
                { "about", this.on_about_action },
                { "preferences", this.on_preferences_action },
                { "quit", this.quit }
            };
            this.add_action_entries (action_entries, this);
            this.set_accels_for_action ("app.quit", {"<primary>q"});
        }

        public override void activate () {
            base.activate ();
            var win = this.active_window;
            if (win == null) {
                win = new MetaballsTest.Window (this);
            }
            win.present ();
        }

        private void on_about_action () {
            string[] developers = { "Alex" };
            var about = new Adw.AboutWindow () {
                transient_for = this.active_window,
                application_name = "metaballs-test",
                application_icon = "org.gnome.Example",
                developer_name = "Alex",
                version = "0.1.0",
                developers = developers,
                copyright = "© 2023 Alex",
            };

            about.present ();
        }

        private void on_preferences_action () {
            message ("app.preferences action activated");
        }
    }
}
