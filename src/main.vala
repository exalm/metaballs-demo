
int main (string[] args) {
    typeof (MetaballsTest.Spinner).ensure ();

    var app = new MetaballsTest.Application ();
    return app.run (args);
}
