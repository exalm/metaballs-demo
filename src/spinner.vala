public class MetaballsTest.Spinner : Gtk.Widget {
    private const string SHADER = """
uniform vec4 color;
uniform float progress;
uniform vec2 pointer;

float computeBall(vec2 uv, vec2 center, float radius) {
    float d = distance(center, uv);

    if(d <= radius)
        return 1.0 - smoothstep(0.0, radius, d);

    return 0.0;
}

void mainImage(out vec4 fragColor, in vec2 fragCoord, in vec2 resolution, in vec2 uv) {
    float total = 0;

    for (int i = -15; i <= 15; i++)
        total += computeBall(uv, vec2(0.5 + 0.02 * i * progress, 0.5 + 0.02 * i * progress), 0.15);

    total += computeBall(uv, pointer, 0.27);

    float alpha = smoothstep(0.4 - fwidth(total), 0.4, total);

    fragColor = color * alpha;
}
""";

    private Gsk.GLShader? shader;
    private bool shader_compiled;

    private Adw.SpringAnimation animation;

    private Graphene.Vec2 pointer_coords;

    construct {
        animation = new Adw.SpringAnimation (this, 0, 1, new Adw.SpringParams (0.6, 2, 150), new Adw.CallbackAnimationTarget (queue_draw));
        animation.epsilon = 0.005;

        animation.done.connect (() => {
            if (!get_mapped ())
                return;

            if (animation.value_from < 0.5) {
                animation.value_from = 1;
                animation.value_to = 0;
            } else {
                animation.value_from = 0;
                animation.value_to = 1;
            }

            animation.play ();
        });

        var controller = new Gtk.EventControllerMotion ();
        controller.motion.connect ((x, y) => {
            pointer_coords.init ((float) x / get_width (), 1.0f - (float) y / get_height ());
            queue_draw ();
        });
        add_controller (controller);
    }

    protected override void map () {
        base.map ();

        animation.play ();
    }

    protected override void measure (Gtk.Orientation orientation, int for_size, out int min, out int nat, out int min_baseline, out int nat_baseline) {
        min = nat = 256;
        min_baseline = nat_baseline = -1;
    }

    protected override void snapshot (Gtk.Snapshot snapshot) {
        var color = get_color ();

        ensure_shader ();

        if (shader_compiled) {
            Graphene.Vec4 colorv = {};
            Graphene.Rect bounds = {};
            var snapshot2 = new Gtk.Snapshot ();

            colorv.init (color.red, color.green, color.blue, color.alpha);
            bounds.init (0, 0, get_width (), get_height ());

            snapshot2.push_gl_shader (
                shader,
                bounds,
                shader.format_args (
                    color: colorv,
                    progress: (float) animation.value,
                    pointer: pointer_coords
                )
            );

            snapshot2.pop ();

            var node = snapshot2.free_to_node ();
            snapshot.append_node (node);
        } else {
            // TODO: fallback
        }
    }

    private void ensure_shader () {
        if (shader != null)
            return;

        var bytes = new Bytes.static ((uint8[]) SHADER);
        shader = new Gsk.GLShader.from_bytes (bytes);

        var renderer = get_native ().get_renderer ();

        try {
            shader_compiled = shader.compile (renderer);
        }
        catch (Error e) {
            critical (e.message);
            /* We don't really care about errors here */
            shader_compiled = false;
        }
    }
}
